package copa;

import copa.Substituicao;
import copa.Jogo;
import entidade.Jogador;
import entidade.Tecnico;
import excessoes.NeedsMorePlayersException;
import principal.FuncoesUteis;

import java.util.LinkedList;

public class Time{
    private int pontos;
    private String nome;
    private LinkedList<Jogador> escalacao;
    private LinkedList<Jogador> reservas;
    private LinkedList<Jogador> jogadores;
    private Tecnico tecnico;
    private int gols;
    Time(String nome, LinkedList<Jogador> jogadores, Tecnico tecnico){
        this.pontos = 0;
        this.gols = 0;
        this.jogadores = jogadores;
        this.tecnico = tecnico;
        this.nome = nome;
    }
    public Time(String nome){
        this.pontos = 0;
        this.gols = 0;
        this.nome = nome;
        this.escalacao = new LinkedList<Jogador>();
        this.reservas = new LinkedList<Jogador>();
        this.jogadores = new LinkedList<Jogador>();
    }
    /**
     * gera a escalacao do time, e coloca os outros jogadores na reserva
     * @exception NeedsMorePlayersException se o time tiver menos de 11 jogadores
     */
    public boolean gerarEscalacao() throws NeedsMorePlayersException{
        int index, i;
        this.reservas.clear();
        this.escalacao.clear();
        if(this.jogadores.size() >= 11){
            for(i = 0; i < this.jogadores.size() - 1; i++){
                this.reservas.add(this.jogadores.get(i));
            }
            for(i = 0; i < 11; i++){
                index = FuncoesUteis.gerarInteiro(0, this.reservas.size() - 1);
                this.escalacao.add(this.reservas.get(index));
                this.reservas.remove(this.reservas.get(index));
            }
            return true;
        }
        return false;
    }
    /**
     * Adiciona um tecnico ao time
     * @param tecnico novo tecnico do time
     */
    public void addTecnico(Tecnico tecnico){
        this.tecnico = tecnico;
        tecnico.setTime(this);
    }
    /**
     * Adiciona um tecnico ao time
     * @param jogador novo tecnico do time
     */
    public void addJogador(Jogador jogador){
        this.jogadores.add(jogador);
        jogador.setTime(this);
    }
    /**
     * soma i aos pontos do time
     * @param i pontos que vao ser somados
     */
    public void somarPontos(int i){
        this.pontos += i;
    }
    /**
     * Substitui um jogador pelo outro e retorna uma substituicao, ou nulo se algo deu errado. Tambem adiciona essa substituicao no jogo em que a substituicao aconteceu
     * @param atual Jogador que vai sair
     * @param substituto jogador que vai entrar
     * @param jogo jogo em que a substituicao aconteceu
     */
    public Substituicao substituir(Jogador atual, Jogador substituto, Jogo jogo, String tempo){
        if(this.escalacao.contains(atual) && this.reservas.contains(substituto)){
            this.escalacao.remove(atual);
            this.escalacao.add(substituto);
            this.reservas.remove(substituto);
            this.reservas.add(atual);
            Substituicao substituicao = new Substituicao(atual, substituto, jogo, tempo);
            jogo.getSubstituicoes().add(substituicao);
            return substituicao;
        }else 
        return null;
    }
    /**
     * Substitui um jogador pelo outro e retorna uma substituicao, ou nulo se algo deu errado. Tambem adiciona essa substituicao no jogo em que a substituicao aconteceu
     * @param atual Jogador que vai sair
     * @param substituto jogador que vai entrar
     * @param jogo jogo em que a substituicao aconteceu
     */
    public Substituicao substituir(Jogador atual, Jogador substituto, Jogo jogo, int tempo){
        if(this.escalacao.contains(atual) && this.reservas.contains(substituto)){
            this.escalacao.remove(atual);
            this.escalacao.add(substituto);
            this.reservas.remove(substituto);
            this.reservas.add(atual);
            Substituicao substituicao = new Substituicao(atual, substituto, jogo, tempo);
            jogo.getSubstituicoes().add(substituicao);
            return substituicao;
        }else 
        return null;
    }
    public void addGol(){
        this.gols++;
    }
    /**Getters and Setters */
        /**
         * @return the gols
         */
        public int getGols() {
            return gols;
        }
        /**
         * @return the pontos
         */
        public int getPontos() {
            return pontos;
        }
        /**
         * @return the reservas
         */
        public LinkedList<Jogador> getReservas() {
            return reservas;
        }
        /**
         * @return Retorna o jogador reserva na posicao i da lista
         * @param i a posicao do reserva
         */
        public Jogador getReserva(int i) {
            return reservas.get(i);
        }
        /**
         * @return Retorna um jogador escalado especifico do vetor
         */
        public Jogador getEscalado(int i) {
            return this.escalacao.get(i);
        }
        /**
         * @return Retorna o vetor escalacao do time
         */
        public LinkedList<Jogador> getEscalacao() {
            return escalacao;
        }
        /**
         * @return Nome do time
         */
        public String getNome() {
            return nome;
        }
        /**
         * @return Tecnico do time
         */
        public Tecnico getTecnico() {
            return tecnico;
        }
        /**
         * @param escalacao Coloca um jogador na escalação do time
         */
        public void setJogador(Jogador jogador) {
            this.escalacao.add(jogador);
        }
        /**
         * @param nome Seta o nome do time
         */
        public void setNome(String nome) {
            this.nome = nome;
        }
        /**
         * @param tecnico Seta o tecnico do time
         */
        public void setTecnico(Tecnico tecnico) {
            this.tecnico = tecnico;
        }
        /**
         * @param jogadores the jogadores to set
         */
        public void setJogadores(LinkedList<Jogador> jogadores) {
            this.jogadores = jogadores;
        }

        public LinkedList<Jogador> getJogadores(){
            return jogadores;
        }

        public Jogador getJogador(int i){
            return this.escalacao.get(i);
        }

        public void addEscalado(Jogador escalado){
            this.escalacao.add(escalado);
        }
}