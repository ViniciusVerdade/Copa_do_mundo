package copa;

import copa.Substituicao;
import copa.Cartao;
import copa.Estadio;
import copa.Gol;
import copa.Jogo;
import entidade.Jogador;
import entidade.Arbitro;
import excessoes.NoGameException;
import excessoes.NoPlayerException;
import excessoes.NoTeamException;
import principal.FuncoesUteis;
import java.util.LinkedList;


public class Jogo {
    //antes a rodada tava no campeonato, acho melhor ficar no jogo mesmo porque é bem mais útil aqui
    private String fase;
    private int golsT1;
    private int golsT2;
    private int dia;
    private int mes;
    private int PgolsT1;
    private int PgolsT2;
    private String horario;
    private Time time1;
    private Time time2;
    private Estadio estadio;
    private LinkedList<Gol> gols;
    private LinkedList<Cartao> cartoes;
    private LinkedList<Arbitro> arbitragem;
    private LinkedList<Substituicao> substituicoes;
    /**
     * Cria um jogo. Seta a quantidade de gols realizados para um numero negativo para indicar que o(s) jogo/penaltis nao aconteceram
     * @param fase do campeonato em que o jogo foi realizado
     * @param dia em que o jogo aconteceu
     * @param mes em que o jogo aconteceu
     * @param horario em que o jogo aconteceu
     * @param time1 que vai jogar o jogo
     * @param time2 que vai jogar o jogo 
     * @param estadio em que o jogo vai acontecer
     * @param arbitragem que vai apitar o jogo
     */
    public Jogo(String fase, int dia, int mes, String horario, Time time1, Time time2, Estadio estadio, LinkedList<Arbitro> arbitragem){
        this.gols = new LinkedList<Gol>();
        this.cartoes = new LinkedList<Cartao>();
        this.substituicoes = new LinkedList<Substituicao>();
        this.estadio = estadio;
        this.golsT1 = -1;
        this.golsT2 = -1;
        this.fase = fase;
        this.dia = dia;
        this.mes = mes;
        this.horario = horario;
        this.time1 = time1;
        this.time2 = time2;
        this.arbitragem = arbitragem;
        this.PgolsT1 = -1;
        this.PgolsT2 = -1;
    }
        /**
     * Cria um jogo. Seta a quantidade de gols realizados para um numero negativo para indicar que o(s) jogo/penaltis nao aconteceram
     * @param fase do campeonato em que o jogo foi realizado. 3 para Quartas, 2 para Semi, 1 para final, 0 para 3 lugar, 4 para oitavas e outra coisa para fase de grupos.
     * @param dia em que o jogo aconteceu
     * @param mes em que o jogo aconteceu
     * @param horario em que o jogo aconteceu
     * @param time1 que vai jogar o jogo
     * @param time2 que vai jogar o jogo 
     * @param estadio em que o jogo vai acontecer
     * @param arbitragem que vai apitar o jogo
     */
    Jogo(int fase, int dia, int mes, String horario, Time time1, Time time2, Estadio estadio, LinkedList<Arbitro> arbitragem){
        this.gols = new LinkedList<Gol>();
        this.cartoes = new LinkedList<Cartao>();
        this.substituicoes = new LinkedList<Substituicao>();
        this.estadio = estadio;
        this.golsT1 = -1;
        this.golsT2 = -1;
        if(fase == 3){
            this.fase = "Quartas de final";
        }else if(fase == 2){
            this.fase = "Semifinais";
        }else if(fase == 1){
            this.fase = "Final";
        }else if(fase == 0){
            this.fase = "Disputa pelo 3 lugar";
        }else if(fase == 4){
            this.fase = "Oitavas de final";
        }else{
            this.fase = "Fase de Grupos";
        }
        this.dia = dia;
        this.mes = mes;
        this.horario = horario;
        this.time1 = time1;
        this.time2 = time2;
        this.arbitragem = arbitragem;
        this.PgolsT1 = -1;
        this.PgolsT2 = -1;
    }
    /**
     * Simula um jogo entre os dois times. Precisa ter o jogo inteiramente preenchido
     * @param eliminatoria verdadeiro se o jogo nao puder terminar em empate.
     * @exception NullPointerException se o jogo nao foi devidamente preenchido(Algum dos times faltando, nao tiver o numero certo de arbitros no jogo)
     */
    
    public void simularJogo(boolean eliminatoria) throws NullPointerException{
        try{
            this.golsT1 = 0;
            this.golsT2 = 0;
            int i;
            int qntGols;
            int qntCartoes;
            int qntSubstituicoes;
            int subsT1 = 0;
            int subsT2 = 0;
            int tempo;
            Gol gol;
            Jogador autor;
            Arbitro arbitro;
            for(tempo = 0; tempo < 2; tempo++){
                qntGols = FuncoesUteis.gerarInteiro(0, FuncoesUteis.gerarInteiro(3, FuncoesUteis.gerarInteiro(5, 10)))/2;
                for(i = 0; i < qntGols; i++){
                    if(FuncoesUteis.gerarInteiro(0, 1) == 0){
                        autor = time1.getEscalado(FuncoesUteis.gerarInteiro(0, time1.getEscalacao().size() - 1));
                        this.golsT1++;
                    }else{
                        autor = time2.getEscalado(FuncoesUteis.gerarInteiro(0, time2.getEscalacao().size() - 1));
                        this.golsT2++;
                    }
                    gol = autor.fazerGol(this, tempo);
                    this.gols.addLast(gol);
                }
                qntCartoes = FuncoesUteis.gerarInteiro(0, FuncoesUteis.gerarInteiro(4, 7))/2;
                for(i = 0; i < qntCartoes; i++){
                    if(FuncoesUteis.gerarInteiro(0, 1) == 0){
                        autor = time1.getEscalado(FuncoesUteis.gerarInteiro(0, time1.getEscalacao().size() - 1));
                    }else{
                        autor = time2.getEscalado(FuncoesUteis.gerarInteiro(0, time2.getEscalacao().size() - 1));
                    }
                    if(autor.AmarelosPorJogo(this) > 0 || FuncoesUteis.gerarInteiro(0, 4) == 4){
                        arbitro = arbitragem.get(FuncoesUteis.gerarInteiro(0, 5));
                        arbitro.darCartaoVermelho(autor, this, tempo);
                        autor.getTime().getEscalacao().remove(autor);
                    }else{
                        arbitro = arbitragem.get(FuncoesUteis.gerarInteiro(0, 5));
                        arbitro.darCartaoAmarelo(autor, this, tempo);
                    }
                }
                Jogador entra;
                Jogador sai;
                qntSubstituicoes = FuncoesUteis.gerarInteiro(0, 3) - subsT1;
                subsT1 += qntSubstituicoes;
                for(i = 0; i < qntSubstituicoes; i++){
                    entra = time1.getReserva(FuncoesUteis.gerarInteiro(0, time1.getReservas().size() - 1));
                    sai = time1.getEscalado(FuncoesUteis.gerarInteiro(0, time1.getEscalacao().size() - 1));
                    time1.substituir(sai, entra, this, tempo);
                }
                qntSubstituicoes = FuncoesUteis.gerarInteiro(0, 3) - subsT2;
                subsT2 += qntSubstituicoes;
                for(i = 0; i < qntSubstituicoes; i++){
                    entra = time2.getReserva(FuncoesUteis.gerarInteiro(0, time2.getReservas().size() - 1));
                    sai = time2.getEscalado(FuncoesUteis.gerarInteiro(0, time2.getEscalacao().size() - 1));
                    time2.substituir(sai, entra, this, tempo);
                }
            }
            //fazer prorrogação e pênaltis se for necessário
            if(eliminatoria == true && this.vencedor() == null){
                int golsprorrog = FuncoesUteis.gerarInteiro(0, FuncoesUteis.gerarInteiro(2, 3));
                for(i = 0; i < golsprorrog; i++){
                    if(FuncoesUteis.gerarInteiro(0, 1) == 0){
                        autor = time1.getEscalado(FuncoesUteis.gerarInteiro(0, time1.getEscalacao().size() - 1));
                        this.golsT1++;
                    }else{
                        autor = time2.getEscalado(FuncoesUteis.gerarInteiro(0, time2.getEscalacao().size() - 1));
                        this.golsT2++;
                    }
                    gol = autor.fazerGol(this, "Prorrogacao");
                    this.gols.addLast(gol);
                }
                if(this.vencedor() == null){
                    PgolsT1 = 0;
                    PgolsT2 = 0;
                    i = 5;
                    while(i != 0){
                        if(FuncoesUteis.gerarInteiro(0, 3) < 3){
                            PgolsT1++;
                            if(PgolsT1 > i + PgolsT2){
                                break;
                            }
                        }
                        if(FuncoesUteis.gerarInteiro(0, 3) < 3){
                            PgolsT2++;
                            if(PgolsT2 > i + PgolsT1){
                                break;
                            }
                        }
                    i--;
                    }
                    if(PgolsT1 == PgolsT2){
                        while(true){
                            if(FuncoesUteis.gerarInteiro(0, 3) < 3){
                                PgolsT1++;        
                            }
                            if(FuncoesUteis.gerarInteiro(0, 3) < 3){
                                PgolsT2++;
                            }
                            if(PgolsT2 != PgolsT1){
                                break;
                            }
                        }
                    }
                }
            }
            System.out.println("\nO resultado do jogo foi " + time1.getNome() + " " + this.golsT1 + " x " + this.golsT2 + " " + time2.getNome());
            if(eliminatoria == true && this.golsT1 == this.golsT2){
                System.out.println(" \nO jogo foi decidido nos penaltis, com resultado " + time1.getNome() + " " + this.PgolsT1 + " x " + time2.getNome() + " " + this.PgolsT2);
            }
        }catch(NoTeamException e){
            throw new NullPointerException();
        }catch(NoGameException e){
            throw new NullPointerException();
        }catch(NoPlayerException e){
            throw new NullPointerException();
        }
    }



    /**
     * Retorna o time que ganhou o jogo.
     * @return o time ganhador caso algum tenha ganho ou nulo caso o jogo nao tenha acontecido ou tenha sido um empate
     */
    public Time vencedor(){
        if(golsT1 < 0 || golsT2 < 0){
            System.out.println("\nO jogo ainda nao aconteceu");
            return null;
        }else if(golsT1 > golsT2){
            return time1;
        }else if(golsT2 > golsT1){
            return time2;
        }else if(PgolsT1 >= 0){
            if(PgolsT1 > PgolsT2){
                return time1;
            }else{
                return time2;
            }
        }else{
            return null;
        }
    }

    /**
     * Retorna o time que perdeu o jogo.
     * @return o time perdedor caso algum tenha ganho ou nulo caso o jogo nao tenha acontecido ou tenha sido um empate
     */
    public Time perdedor(){
        if(golsT1 < 0 || golsT2 < 0){
            System.out.println("\nO jogo ainda nao aconteceu");
            return null;
        }else if(golsT1 > golsT2){
            return time2;
        }else if(golsT2 > golsT1){
            return time1;
        }else if(PgolsT1 >= 0){
            if(PgolsT1 > PgolsT2){
                return time2;
            }else{
                return time1;
            }
        }else
        return null;

    }

    /**
     * Retorna uma string padrao que mostra as informacoes basicas do jogo.
     */
    public String apresentarJogo(){
        String apresentacao = "\n";
        apresentacao += this.getTime1().getNome();
        apresentacao += " x " + this.getTime2().getNome();
        apresentacao += " no dia " + this.getDia() + "/" + this.getMes();
        apresentacao += " as " + this.getHorario() + " no estadio " + this.estadio.getNome();
        return apresentacao;
    }
    
    /**
     * Retorna uam string do relatorio do jogo depois que realizado
     * @return
     */
    public String relatorio(){
        if(this.golsT1 < 0){
            return null;
        }
        int i;
        String relatorio = "\r\n";
        Gol gol;
        Cartao cartao;
        Substituicao substituicao;
        relatorio += this.getTime1().getNome() + " " + this.golsT1 + " x " + golsT2 + " " + getTime2().getNome();
        relatorio += "\r\nGols: ";
        for(i = 0;i < this.gols.size(); i++){
            gol = this.gols.get(i);
            relatorio += "\r\nO jogador " + gol.getAutor().getNome() + " fez um gol no(a) " + gol.getTempo();
        }
        relatorio += "\r\nCartoes: ";
        for(i = 0; i < this.cartoes.size(); i++){
            cartao = this.cartoes.get(i);
            relatorio += "\r\nO jogador " + cartao.getJogador().getNome() + " recebeu um cartao " + cartao.getCor() + " no(a) " + cartao.getTempo();
        }
        relatorio += "\n\rSubstituicoes: ";
        for(i = 0; i < this.substituicoes.size(); i++){
            substituicao = this.substituicoes.get(i);
            relatorio += "\r\nO jogador " + substituicao.getEntrou().getNome() + " entrou no lugar do jogador " + substituicao.getSaiu().getNome() + " no(a) " + substituicao.getTempo();
        }
        relatorio += "\r\nO time ganhador foi o " + this.vencedor().getNome();
        if(PgolsT1 >= 0){
            relatorio += ", com decisao nos penaltis com placar " + this.getTime1().getNome() + " " + this.PgolsT1 + " x " + this.PgolsT2 + " " + getTime2().getNome();
        }
    return relatorio;
    }

    /*Getters and Setters*/
        /**
         * @return retorna o mes
         */
        public int getMes() {
            return mes;
        }
        /**
         * @return a fase em que o jogo aconteceu.
         */
        public String getFase() {
            return fase;
        }
        /**
         * @return retorna a lista de cartoes
         */
        public LinkedList<Cartao> getCartoes() {
            return cartoes;
        }
        /**
         * @return o dia do jogo
         */
        public int getDia() {
            return dia;
        }
        /**
         * @return o estadio do jogo
         */
        public Estadio getEstadio() {
            return estadio;
        }
        /**
         * @return O horario do jogo
         */
        public String getHorario() {
            return horario;
        }
        /**
         * @return o primeiro time
         */
        public Time getTime1() {
            return time1;
        }
        /**
         * @return o segundo time
         */
        public Time getTime2() {
            return time2;
        }
        /**
         * @return a arbitragem
         */
        public Arbitro getArbitro(int i) {
            return arbitragem.get(i);
        }
        
        /**
         * @param arbitragem o arbitragem to set
         */
        public void setArbitragem(LinkedList<Arbitro> arbitragem) {
            this.arbitragem = arbitragem;
        }

        public int getGolsT1(){
            return golsT1;
        }

        public int getGolsT2(){
            return golsT2;
        }
        public LinkedList<Gol> getGols(){
            return gols;
        }
        public LinkedList<Substituicao> getSubstituicoes(){
            return substituicoes;
        }

        
}


