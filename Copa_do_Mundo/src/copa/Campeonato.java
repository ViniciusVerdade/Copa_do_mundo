package copa;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;

import entidade.Arbitro;
import excessoes.NeedsMorePlayersException;
import excessoes.NeedsMoreTeamsException;
import excessoes.IncompleteChampionshipException;
import principal.FuncoesUteis;

public class Campeonato{
    private String nome;
    private LinkedList<Jogo> jogos;
    private LinkedList<Time> times;
    private LinkedList<Time> eliminatorias;
    private LinkedList<Grupo> grupos;
    private LinkedList<Estadio> estadios;
    private LinkedList<Arbitro> arbitros;
    private LinkedList<Time> podio;
    public Campeonato(String nome){
        this.nome = nome;
        jogos = new LinkedList<Jogo>();
        times = new LinkedList<Time>();
        eliminatorias = new LinkedList<Time>();
        podio = new LinkedList<Time>();
        grupos = new LinkedList<Grupo>();
        estadios = new LinkedList<Estadio>();
        arbitros = new LinkedList<Arbitro>();
        System.out.println("Campeonato " + this.nome + " criado!");
    }

    /**
     * gera aleatoriamente os grupos do campeonato com os times ja registrados. Precisa ter 32 times e nao podem existir grupos no campeonato
     * @exception NeedsMoreTeamsException se o numero de times for insuficiente
     */
    public void gerarGrupos() throws NeedsMoreTeamsException{
        try{
            this.grupos.clear();
            int timeindex;
            int i;
            int j;
            LinkedList<Time> aux = new LinkedList<Time>();
            for(i = 0; i < this.times.size(); i ++){
                aux.add(this.times.get(i));
            }
            for(i = 0; i < 8; i++){
                Grupo grupo = new Grupo(i);
                for(j = 0; j < 4; j++){
                    timeindex = FuncoesUteis.gerarInteiro(0, aux.size() - 1);
                    grupo.addTime(aux.get(timeindex));
                    aux.remove(timeindex);
                }
                this.grupos.addLast(grupo);
            }
        }catch(IllegalArgumentException e){
            throw new NeedsMoreTeamsException();
        }
    }
    /**
     * Simula a fase de grupos do campeonato. Pega cada grupo, e cria uma partida entre cada um dos times, num total de 6 partidas. O time ganhador ganha 3 pontos, e em caso de empate ambos os times ganham 1 ponto. Depois de simular todos os jogos e adicionar os pontos, reorganiza os times usando quicksort pela quantidade de pontos, depois passa de dois em dois times vendo se a quantidade de pontos entre eles sao iguais, e caso sejam desempata pelo numero de gols.
     * @exception IncompleteChampionshipException caso falte grupos ou arbitros
     * @exception NeedsMoreTeamsException caso falte times nos grupos
     * @exception NeedsMorePlayersException caso falte jogadores para gerar a escalacao
     * @throws IOException 
     */
    public LinkedList<Time> simularFaseGrupos() throws IncompleteChampionshipException, NeedsMoreTeamsException, NeedsMorePlayersException, IOException {
        try{
            this.jogos.clear();
            String[] horarios = {"07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00"};
            String horario;
            int i, j, k, x;
            int indice;
            Grupo grupo;
            Estadio estadio;
            LinkedList<Arbitro> arbitragem = new LinkedList<Arbitro>();
            LinkedList<Arbitro> auxArbitros = new LinkedList<Arbitro>();
            Jogo jogo;
            
            FileWriter arquivoRelatorio;
            PrintWriter gravarRelatorio;
            
            arquivoRelatorio = new FileWriter("./" + this.getNome() +"_fase_grupos"+ ".txt");
            gravarRelatorio = new PrintWriter(arquivoRelatorio);
            
            for(i = 0; i < 8; i ++){
                grupo = grupos.get(i);
                for(j = 0; j < 4; j ++){
                    for(k = j + 1; k < 4; k ++){
                        arbitragem.clear();
                        for(x = 0; x < this.arbitros.size(); x++){
                            auxArbitros.add(this.arbitros.get(x));
                        }
                        for(x = 0; x < 6; x++){
                            indice = FuncoesUteis.gerarInteiro(0, auxArbitros.size() - 1);
                            arbitragem.addLast(auxArbitros.get(indice));
                            auxArbitros.remove(indice);
                        }
                        grupo.getTime(j).gerarEscalacao();
                        grupo.getTime(k).gerarEscalacao();
                        estadio = this.estadios.get(FuncoesUteis.gerarInteiro(0, this.estadios.size() - 1));
                        horario = horarios[FuncoesUteis.gerarInteiro(0, 8)];
                        jogo = new Jogo("Fase de Grupos", FuncoesUteis.gerarInteiro(1, 30), 6, horario, grupo.getTime(j), grupo.getTime(k), estadio, arbitragem);
                        jogo.simularJogo(false);
                        jogos.addLast(jogo);
                    }
                }
            }
            for(i = 0; i < jogos.size(); i++){
                if(jogos.get(i).vencedor() == null){
                    jogos.get(i).getTime1().somarPontos(1);
                    jogos.get(i).getTime2().somarPontos(1);
                }else{
                    jogos.get(i).vencedor().somarPontos(3);
                }
            }
            for(i = 0; i < 8; i++){
                FuncoesUteis.quicksortColocacao(grupos.get(i).getTimes());
            }
            /*essa parte dá pra tirar */
            gravarRelatorio.println("Times vencedores: ");
            for(i = 0; i < 8; i ++){
                gravarRelatorio.println("Grupo " + grupos.get(i).getNome());
                for(j = 0; j < 4; j ++){
                    gravarRelatorio.print(j + 1 + " Lugar: ");
                    gravarRelatorio.println(grupos.get(i).getTime(j).getNome() + " Pontos: " + grupos.get(i).getTime(j).getPontos() + " Gols: " + grupos.get(i).getTime(j).getGols());
                }
            }
            /*acabou a parte que dá pra tirar*/
            LinkedList<Time> times = new LinkedList<Time>();
            for(i = 0; i < 8; i++){
                times.add(grupos.get(i).getTime(0));
                times.add(grupos.get(i).getTime(1));
                grupos.get(i).getTimes().remove(3);
                grupos.get(i).getTimes().remove(2);
            }
            gravarRelatorio.close();
            return times;
        }
        catch(IllegalArgumentException e){
            throw new IncompleteChampionshipException();
        }
        catch(IndexOutOfBoundsException e){
            throw new NeedsMoreTeamsException();
        }
    }

    /**
     * Acha todos os jogos do time1
     * @param time1 time
     * @return uma lista de jogos entre o time1 e time2
     */
    public LinkedList<Jogo> acharJogo(Time time1){
        int i;
        Time auxtime1, auxtime2;
        Jogo auxjogo;
        LinkedList<Jogo> jogos = new LinkedList<Jogo>();
        try{
            for(i = 0; i < this.jogos.size(); i++){
                auxjogo = this.jogos.get(i);
                auxtime1 = auxjogo.getTime1();
                auxtime2 = auxjogo.getTime2();
                if((time1.equals(auxtime1)) || (time1.equals(auxtime2))){
                    jogos.add(auxjogo);   
                }
            }
            return jogos;
        }catch(IndexOutOfBoundsException e){
            return null;
        }
    }

    /**
     * Acha todos os jogos entre o time1 e o time2
     * @param time1 primeiro time
     * @param time2 segundo time
     * @return uma lista de jogos entre o time1 e time2
     */
    public LinkedList<Jogo> acharJogo(Time time1, Time time2){
        int i;
        Time auxtime1, auxtime2;
        Jogo auxjogo;
        LinkedList<Jogo> jogos = new LinkedList<Jogo>();
        try{
            for(i = 0; i < this.jogos.size(); i++){
                auxjogo = this.jogos.get(i);
                auxtime1 = auxjogo.getTime1();
                auxtime2 = auxjogo.getTime2();
                if((time1.equals(auxtime1) && time2.equals(auxtime2)) || (time1.equals(auxtime2) && time2.equals(auxtime1))){
                    jogos.add(auxjogo);   
                }
            }
            return jogos;
        }catch(IndexOutOfBoundsException e){
            return null;
        }
    }
        /**
     * Acha todos os jogos daquele indice
     * @param int indice do jogo
     * @return uma lista de jogos naquele indice
     */
    public LinkedList<Jogo> acharJogo(int indice){
        LinkedList<Jogo> jogos = new LinkedList<Jogo>();
        try{
            jogos.add(this.jogos.get(indice));
            return jogos;
        }
        catch(IndexOutOfBoundsException e){
            System.out.println("Indice do jogo foi informado errado ou jogo nao existe!");
            return null;
        }
    }
        /**
     * Acha todos os jogos que aconteceram no dia e mes enviados
     * @param dia 
     * @param time2 segundo time
     * @return uma lista de jogos entre o time1 e time2
     */
    public LinkedList<Jogo> acharJogo(int dia, int mes){
        int i;
        Jogo jogo;
        LinkedList<Jogo> jogos = new LinkedList<Jogo>();
        try{
            for(i = 0; i < this.jogos.size(); i++){
                jogo = this.jogos.get(i);
                if(jogo.getDia() == dia && jogo.getMes() == mes){
                    jogos.add(this.jogos.get(i));
                }
            }
            return jogos;
        }catch(IndexOutOfBoundsException e){
            System.out.println("Indice do jogo foi informado errado ou jogo nao existe!");
            return null;
        }
    }
    /**
     * Simula a segunda fase da copa
     * @throws NeedsMorePlayersException
     * @throws IOException 
     */
    public void simularSegundafase() throws NeedsMorePlayersException, IOException{
        int i, j, k, x, indice, dia = 0;
        String[] horarios = {"07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00"};
        String horario;
        /*Da pra tirar essa parte*/
        FileWriter arquivoRelatorio = new FileWriter("./" + this.getNome() +"_segunda_fase"+ ".txt");
        PrintWriter gravarRelatorio = new PrintWriter(arquivoRelatorio);
        gravarRelatorio.println("Times participantes da segunda fase: ");
        for(j = 0; j < 16; j++){
            gravarRelatorio.println(eliminatorias.get(j).getNome());
        }
        /*cabo a parte que pode tira*/
        Time time1, time2;
        LinkedList<Time> auxTimes = new LinkedList<Time>();
        Estadio estadio;
        Grupo grupo1, grupo2;
        LinkedList<Arbitro> arbitragem = new LinkedList<Arbitro>();
        LinkedList<Arbitro> auxArbitros = new LinkedList<Arbitro>();
        
        for(i = 0; i < 8; i ++){
            auxArbitros.clear();
            //parte de pegar arbitros
            for(j = 0; j < 6; j++){
                auxArbitros.add(this.arbitros.get(j));
            }
            for(x = 0; x < 6; x ++){
                indice = FuncoesUteis.gerarInteiro(0, auxArbitros.size() - 1);    
                arbitragem.addLast(auxArbitros.get(indice));
                auxArbitros.remove(indice);
            }

            grupo1 = grupos.get(i);
            time1 = grupo1.getTime(0);
            if(i < 7){
                grupo2 = grupos.get(i + 1);
            }else{
                grupo2 = grupos.get(0);
            }
            time2 = grupo2.getTime(1);
            time1.gerarEscalacao();
            time2.gerarEscalacao();
            estadio = this.estadios.get(FuncoesUteis.gerarInteiro(0, this.estadios.size() - 1));
            horario = horarios[FuncoesUteis.gerarInteiro(0, 8)];
            dia += FuncoesUteis.gerarInteiro(0, 2);
            Jogo jogo = new Jogo("Oitavas de final", dia, 7, horario, time1, time2, estadio, arbitragem);
            jogo.simularJogo(true);
            jogos.addLast(jogo);
            this.eliminatorias.remove(jogo.perdedor());
        }
        /*da pra tira os sysout aqui mas so os sysout*/
        gravarRelatorio.println("Times vencedores das oitavas de final: ");
        auxTimes.clear();
        for(i = 0; i < 8; i++){
            gravarRelatorio.println(eliminatorias.get(i).getNome());
            auxTimes.add(eliminatorias.get(i));
        }
        /*cabo a parte que pode tirar*/

        /*quartas, semi e final*/
        for(i = 3; i > 0; i--){
            x = eliminatorias.size() / 2;
            for(j = 0; j < x; j++){
                auxArbitros.clear();
                //parte de pegar arbitros
                for(k = 0; k < 6; k++){
                    auxArbitros.add(this.arbitros.get(j));
                }
                for(k = 0; k < 6; k++){
                    indice = FuncoesUteis.gerarInteiro(0, auxArbitros.size() - 1);    
                    arbitragem.addLast(auxArbitros.get(indice));
                    auxArbitros.remove(indice);
                }
                
                indice = FuncoesUteis.gerarInteiro(0, auxTimes.size() - 1);
                time1 = auxTimes.get(indice);
                auxTimes.remove(time1);
                
                indice = FuncoesUteis.gerarInteiro(0, auxTimes.size() - 1);
                time2 = auxTimes.get(indice);
                auxTimes.remove(time2);
                
                time1.gerarEscalacao();
                time2.gerarEscalacao();
                
                estadio = this.estadios.get(FuncoesUteis.gerarInteiro(0, this.estadios.size() - 1));
                horario = horarios[FuncoesUteis.gerarInteiro(0, 8)];
                dia += FuncoesUteis.gerarInteiro(0, 2);
                if(x == 2)
                dia += 2;
                Jogo jogo = new Jogo(i, dia, 7, horario, time1, time2, estadio, arbitragem);
                jogo.simularJogo(true);
                jogos.addLast(jogo);
                this.eliminatorias.remove(jogo.perdedor());
            }
            /*pode tirar os sysout só*/
            gravarRelatorio.println("Times vencedores: ");
            auxTimes.clear();
            for(k = 0; k < eliminatorias.size(); k++){
                gravarRelatorio.println(eliminatorias.get(k).getNome());
                auxTimes.add(eliminatorias.get(k));
            }
            if(eliminatorias.size() == 4){
                for(k = 0; k < eliminatorias.size(); k++){
                    this.podio.add(eliminatorias.get(k));
                }
            }
            if(eliminatorias.size() == 2){
                for(k = 0; k < eliminatorias.size(); k++){
                    this.podio.remove(eliminatorias.get(k));
                    this.podio.add(0, eliminatorias.get(k));
                }
            }
        }
        time1 = this.podio.get(2);
        time2 = this.podio.get(3);
        time1.gerarEscalacao();
        time2.gerarEscalacao();
        auxArbitros.clear();
        //parte de pegar arbitros
        for(k = 0; k < 6; k++){
            auxArbitros.add(this.arbitros.get(j));
        }
        for(k = 0; k < 6; k++){
            indice = FuncoesUteis.gerarInteiro(0, auxArbitros.size() - 1);    
            arbitragem.addLast(auxArbitros.get(indice));
            auxArbitros.remove(indice);
        }
        estadio = this.estadios.get(FuncoesUteis.gerarInteiro(0, this.estadios.size() - 1));
        horario = horarios[FuncoesUteis.gerarInteiro(0, 8)];
        dia--;
        Jogo jogo = new Jogo(i, dia, 7, horario, time1, time2, estadio, arbitragem);
        jogo.simularJogo(true);
        jogos.addLast(jogo);

        this.podio.remove(jogo.perdedor());
        // holy
        time1 = this.podio.get(0);
        time2 = this.podio.get(1);
        time1 = this.acharJogo(time1, time2).getLast().vencedor();
        this.podio.remove(time1);
        this.podio.add(0, time1);
        gravarRelatorio.close();
    }

    public String resultadoCopa() throws IOException{
        String resultado = "";
        FileWriter arquivoRelatorio = new FileWriter("./" + this.getNome() +"_resultado"+ ".txt");
        PrintWriter gravarRelatorio = new PrintWriter(arquivoRelatorio);
        resultado += "Primeiro lugar: " + this.podio.get(0).getNome() + "\r\nSegundo lugar: " + this.podio.get(1).getNome() + "\r\nTerceiro Lugar: "  + this.podio.get(2).getNome();
        gravarRelatorio.println(resultado);
        gravarRelatorio.close();
        return resultado;
    }
    /**
         * adiciona um time aos times do campeonato
         * @param time time que vai ser adicionado
         */
        public void addTime(Time time){
            this.times.add(time);
        }
        /**
         * Adiciona um time ao podio do campeonato
         * @param time
         */
        public void addPodio(Time time){
            this.podio.add(time);
        }
        /**
         * @return the times
         */
        public Time getTime(int i) {
            return this.times.get(i);
        }
        /**
         * adiciona um arbitro aos arbitros do campeonato
         * @param arbitro arbitro que vai ser adicionado
         */
        public void adicionarArbitro(Arbitro arbitro) {
            this.arbitros.add(arbitro);
        }
        /**
         * adiciona um jogo aos jogos do campeonato
         * @param jogo jogo que vai ser adicionado
         */
        public void adicionarJogo(Jogo jogo){
            this.jogos.add(jogo);
        }
        /**
         * adiciona um estadio aos estadios do campeonato
         * @param estadio estadio que vai ser adicionado
         */
        public void adicionarEstadio(Estadio estadio){
            this.estadios.add(estadio);
        }
        /**
         * adiciona um grupo aos grupos do campeonato
         * @param grupo grupo que vai ser adicionado
         */
        public void adicionarGrupo(Grupo grupo){
            this.grupos.add(grupo);
        }
    /*getters e setters*/
    /**
     * @return the estadios
     */
    public LinkedList<Estadio> getEstadios() {
        return estadios;
    }
    public String getNome() {
    	return this.nome;
    }
    
    /**
     * @param eliminatorias a lista com os times que foram pras oitavas/quartas/etc
     */
    public void setEliminatorias(LinkedList<Time> eliminatorias) {
        this.eliminatorias = eliminatorias;
    }
    /**
     * @return the times
     */
    public LinkedList<Time> getTimes() {
        return times;
    }
    
    public LinkedList<Jogo> getJogos() {
        return this.jogos;
    }
}