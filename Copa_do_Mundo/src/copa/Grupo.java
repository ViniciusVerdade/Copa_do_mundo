package copa;

import copa.Grupo;

import java.util.LinkedList;
public class Grupo{
    private String nome;
    private LinkedList<Time> times;
    /**
     * Cria um grupo vazio
     * @param nome do grupo
     */
    Grupo(String nome){
        this.times = new LinkedList<Time>();
        this.nome = nome;
    }
    /**
     * Cria um grupo vazio cujo nome corresponde a uma letra do alfabeto, de A ate H, com 0 sendo A, 1 sendo B e etc
     * @param nome
     */
    Grupo(int nome){
        times = new LinkedList<Time>();
        switch(nome){
            case 0:
            this.nome = "A";
            break;
            case 1:
            this.nome = "B";
            break;
            case 2:
            this.nome = "C";
            break;
            case 3:
            this.nome = "D";
            break;
            case 4:
            this.nome = "E";
            break;
            case 5:
            this.nome = "F";
            break;
            case 6:
            this.nome = "G";
            break;
            case 7:
            this.nome = "H";
            break;
        }
    }
    /**
     * Coloca um time na posicao i da lista. Caso o time ja esteja na lista, remove ele antes de colocar de novo
     * @param i indice do time
     * @param time que vai ser inserido no grupo
     * @exception IndexOutOfBoundsException se a posicao enviada e invalida
     */
    public void addPosicao(int i, Time time) throws IndexOutOfBoundsException{
        this.times.remove(time);
        this.times.add(i, time);
    }
    /**
     * adiciona um time ao grupo se a quantidade de times for menor que 4
     * @param time novo time a ser adicionado
      */
    public void addTime(Time time) {
        this.times.addLast(time);
    }
    /*
        /**
         * @return retorna um time especifico
         */
        public Time getTime(int index){
            return times.get(index);
        }
        /**
         * @return the nome
         */
        public String getNome() {
            return nome;
        }
        /**
         * @return the times
         */
        public LinkedList<Time> getTimes() {
            return times;
        }
        /**
         * @param nome the nome to set
         */
        public void setNome(String nome) {
            this.nome = nome;
        }
        /**
         * @param times the times to set
         */
        public void setTimes(LinkedList<Time> times) {
            this.times = times;
        }
        
}