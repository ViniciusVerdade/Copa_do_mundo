package copa;

import entidade.Jogador;

public class Cartao {
    private String cor;
    private Jogo jogo;
    private Jogador jogador;
    private String tempo;
    /**
     * Cria um cartao.
     * @param cor do cartao
     * @param jogo que o cartao foi dado
     * @param jogador que recebeu o cartao
     * @param tempo do jogo em que o cartao foi dado (0 para primeiro tempo, 1 para segundo tempo e 2 para prorrogacao)
     */
    public Cartao(String cor, Jogo jogo, Jogador jogador, int tempo){
        if(tempo == 0){
            this.tempo = "Primeiro Tempo";
        }else if(tempo == 1){
            this.tempo = "Segundo Tempo";
        }else if(tempo == 2){
            this.tempo = "Prorrogacao";
        }
        this.cor = cor;
        this.jogo = jogo;
        this.jogador = jogador;
    }
    /**
     * Cria um cartao.
     * @param cor do cartao
     * @param jogo que o cartao foi dado
     * @param jogador que recebeu o cartao
     * @param tempo do jogo em que o cartao foi dado
     */
    Cartao(String cor, Jogo jogo, Jogador jogador, String tempo){
        this.cor = cor;
        this.tempo = tempo;
        this.jogo = jogo;
        this.jogador = jogador;
    }
    /*Getters e setters*/
        /**
         * @return quando o cartao foi dado
         */
        public String getTempo() {
            return tempo;
        }
        /**
         * pega a cor do cartao
         * @return a cor do cartao
         */
        public String getCor() {
            return cor;
        }
        /**
         * retorna o jogador que recebeu o cartao
         * @return o objeto jogador a qual o cartao ta vinculado
         */
        public Jogador getJogador() {
            return jogador;
        }
        /**
         * retorna o jogo em que o cartao foi dado
         * @return retorna o jogo
         */
        public Jogo getJogo() {
            return jogo;
        }
}