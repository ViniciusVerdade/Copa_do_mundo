package principal;

import java.lang.Math;
import java.util.LinkedList;
import principal.FuncoesUteis;
import copa.Time;

public class FuncoesUteis{
    /**
     * Gera um numero aleatorio >= chao e <= teto
     * @param int chao
     * @param int teto
     * @return retorna o numero aleatorio
     * @exception IllegalArgumentException se o chao ou teto for um numero negativo
     */
    public static int gerarInteiro(int chao, int teto) throws IllegalArgumentException{
        if(chao < 0 || teto < 0){
            throw new IllegalArgumentException();
        }
        int x = (int) (Math.random() * ((teto-chao) + 1)) + chao;
        return x;
    }
    /**
     * retorna verdadeiro se o numero e potencia de 2
     */
    public static boolean isPow2(int numero){
        if((numero & (numero - 1)) == 0){
            return true;
        }
        return false;
    }
    /**
     * Faz um quicksort dos times baseado na pontuacao deles
     * @param times vetor de times
     * @param inicio onde o quicksort comeca
     * @param fim onde o quicksort termina
     */
    public static void quicksortColocacao(LinkedList<Time> times){
        int i, j,k; 
        Time x;
        Time[] time = new Time[4];
        for(i = 0; i < 4; i++){
            time[i] = times.get(i);
        }
        for(i=0;i<4;i++)
        {
            k = 1;
            for(j=0;j<3;j++)
            {
                if(time[j].getPontos() < time[k].getPontos())
                {
                    x = time[j];
                    time[j] = time[k];
                    time[k] = x;
                }
                k++;
            }
        }
        for(i=0;i<4;i++)
        {
            k = 1;
            for(j=0;j<3;j++)
            {
                if(time[j].getGols() < time[k].getGols() && time[j].getPontos() == time[k].getPontos())
                {
                    x = time[j];
                    time[j] = time[k];
                    time[k] = x;
                }
                k++;
            }
        }
        times.clear();
        for(i = 0; i < 4; i++){
            times.addLast(time[i]);
        }
    }
}