package principal;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Scanner;
import java.io.IOException;
import copa.Estadio;
import copa.Campeonato;
import copa.Time;
import copa.Jogo;
import entidade.Jogador;
import entidade.Arbitro;
import entidade.Tecnico;
import excessoes.NeedsMorePlayersException;
import excessoes.NeedsMoreTeamsException;
import excessoes.IncompleteChampionshipException;
import principal.Menu;

public class Menu {
    public static void main(String[] args) throws IOException {
        Scanner ler = new Scanner(System.in); // scanner utilizado para ler as coisas
        FileWriter arquivoRelatorio;
        PrintWriter gravarRelatorio;
        int opcao; // opção do switch case
        
        String nome1, nome2, nome3; // string que será lida
        String text1, text2;// strings auxiliares
        
        int i, j = 0; // contadores
        int aux1, aux2;// auxiliares
        
        LinkedList<Time> timesCriados = new LinkedList<Time>();
        Time time, time1, time2;
      
        Jogador jog; 
        Tecnico tecnico;
        
        LinkedList<Jogo> jogos = new LinkedList<Jogo>();
        LinkedList<Jogo> jogosAux = new LinkedList<Jogo>();
        Jogo partida;
        
        LinkedList<Arbitro> arbitros = new LinkedList<Arbitro>();
        Arbitro arbi;
        LinkedList<Arbitro> arbitrosGenericos = new LinkedList<Arbitro>();
        
        for (i = 0; i < 6; i++) {
            arbi = new Arbitro("arbitro" + (i + 1), "unidade_federativa-" + (i + 1),"funcao" + (i + 1));
            arbitrosGenericos.add(arbi);
        }
        
        LinkedList<Estadio> estadios = new LinkedList<Estadio>();
        Estadio estadio;
        LinkedList<Estadio> estadiosGenericos = new LinkedList<Estadio>();
        
        for (i = 0; i < 4; i++) {
            estadio = new Estadio("estadio" + (i + 1), "unidade_federativa-" + (i + 1));
            estadiosGenericos.add(estadio);
        }
        
        Campeonato copa;
        
        // cadastrando os times da copa no algoritmo
        nome1 = "./src/principal/times_copa.txt";
        timesCriados = cadastroTimes(nome1);

        while (true) {
        	pularLinha();
            System.out.println("\n\n\n\nOpcoes Disponiveis(MENU PRINCIPAL):");
            System.out.println("1- Adicionar time;");
            System.out.println("2- Simular partida especifica;");
            System.out.println("3- Simular Copa do Mundo(sao necessarios 4 arbitros);");
            System.out.println("4- Checar relatorio da partida(Sumula);");
            System.out.println("5- Criar Estadio;");
            System.out.println("6- Criar arbitro para a Copa do Mundo;");
            System.out.println("7- Gerar relatorios das partidas de dia especifico;");
            System.out.println("8- Finalizar programa.");
            System.out.println("Digite abaixo a opcao desejada :");
            opcao = ler.nextInt();
            ler.nextLine();

            switch (opcao) {
            case 1:
                System.out.print("\nDigite o nome do time:");
                nome1 = ler.nextLine();
                time = new Time(nome1);
                for (i = 0; i < 23; i++) {
                    System.out.print("\nDigite a posicao do jogador:");
                    nome2 = ler.nextLine();
                    if (nome2.equals("Goleiro") || nome2.equals("goleiro")) {
                        j++;
                        if (j > 3) {
                            System.out.print("\nO time ja possui 3 goleiros, jogador nao adicionado.\n");
                            i--;
                            continue;
                        }
                    }
                    System.out.print("\nDigite o nome do jogador:");
                    nome3 = ler.nextLine();
                    jog = new Jogador(nome2, nome3);
                    time.addJogador(jog);
                }
                System.out.print("\nDigite o nome do tecnico do time:");
                nome2 = ler.nextLine();// TEM QUE CRIAR O TECNICO @!

                tecnico = new Tecnico(nome2);
                time.addTecnico(tecnico);

                timesCriados.add(time);
                
                System.out.print("\n\n\nTecle Enter para voltar ao menu!");
                ler.nextLine();
                break;
            case 2:
                time1 = null;
                time2 = null;
                estadio = null;
                System.out.print("\nDigite o nome do primeiro time: ");
                nome1 = ler.nextLine();

                time1 = findTime(timesCriados, nome1);

                System.out.print("\nDeseja escolher a escalacao(E) ou gerar automaticamente(A)? (E/A):");
                nome1 = ler.nextLine();

                if (nome1.equals("E")) // CASO O CARA QUEIRA ESCOLHER A ESCALACAO
                {
                    for (i = 0; i < 11; i++) {
                        System.out.print("\nEscreva o nome do jogador escalado:"); // VENDO 11 JOGADORES PARA O TIME
                        text1 = ler.nextLine();
                        for (j = 0; j < 23; j++) {
                            if (time1.getJogador(i).getNome().equals(text1))// CHECANDO SE O JOGADOR DIGITADO EXISTE
                            {
                                jog = time1.getJogador(i);
                                time1.addEscalado(jog);
                            }
                        }
                        if (j == 23) {
                            System.out.println("\nJogador nao existente, por favor, digite um Jogador existente");
                            i--;
                        }
                    }
                } else {
                    if (nome1.equals("A")) {
                        try{
                            time1.gerarEscalacao();
                        }catch(NeedsMorePlayersException e){
                            System.out.println("Quantidade de times insuficiente!");
                        }
                    } else {
                        System.out.println("\nOpcao invalida.");
                    }

                }

                System.out.print("\nDigite o nome do segundo time:");
                nome2 = ler.nextLine();

                time2 = findTime(timesCriados, nome2);

                System.out.print("\nDeseja escolher a escalacao(E) ou gerar automaticamente(A)? (E/A):");
                nome2 = ler.nextLine();

                if (nome2.equals("E")) // CASO O CARA QUEIRA ESCOLHER A ESCALACAO
                {
                    for (i = 0; i < 11; i++) {
                        System.out.print("\nEscreva o nome do jogador escalado:"); // VENDO 11 JOGADORES PARA O TIME
                        text2 = ler.nextLine();
                        for (j = 0; j < 23; j++) {
                            if (time2.getJogador(i).getNome().equals(text2))// CHECANDO SE O JOGADOR DIGITADO EXISTE
                            {
                                jog = time2.getJogador(i);
                                time2.addEscalado(jog);
                            }
                        }
                        if (j == 23) {
                            System.out.println("\nJogador nao existente, por favor, digite um Jogador existente");
                            i--;
                        }
                    }
                } else {
                    if (nome2.equals("A")) {
                        try{
                            time2.gerarEscalacao();
                        }catch(NeedsMorePlayersException e){
                            System.out.println("Quantidade de times insuficiente!");
                        }
                    } else {
                        System.out.println("\nOpcao invalida.");
                    }

                }

                System.out.print("\nDigite o nome do estadio em que a partida sera/foi realizada:");
                nome3 = ler.nextLine();

                estadio = findEstadio(estadios, nome3);

                System.out.print("\nDigite o dia em que a partida sera/foi realizada:");
                aux1 = ler.nextInt();
                ler.nextLine();

                System.out.print("\nDigite o mes em que a partida sera/foi realizada:");
                aux2 = ler.nextInt();
                ler.nextLine();

                System.out.print("\nDigite o horario em que a partida sera/foi realizada:");
                text1 = ler.nextLine();

                partida = new Jogo(" ", aux1, aux2, text1, time1, time2, estadio, arbitrosGenericos);
                partida.simularJogo(true);

                jogos.add(partida);

                System.out.print("\nPartida realizada!");

                System.out.print("\n\n\nTecle Enter para voltar ao menu!");
                ler.nextLine();
                break;

            case 3:
                copa = new Campeonato("Copa do Mundo 2018");
                for(i=0;i<32;i++){
                    copa.addTime(timesCriados.get(i));
                }
                
                System.out.println("\nDeseja utilizar arbitros genericos(G) ou arbitros criados(C) para a Copa do Mundo 2018?(G/C)");
                text1 = ler.nextLine();
                if(text1.equals("G")){
                    for(i=0;i<6;i++){
                        copa.adicionarArbitro(arbitrosGenericos.get(i));
                    }   
                }else if(text1.equals("C")){
                    for(i=0;i<6;i++){
                        copa.adicionarArbitro(arbitros.get(i));
                    }
                }else{
                    System.out.println("\nComo nao foi inserido uma opcao valida, serao utilizados arbitros genericos");
                    for(i=0;i<6;i++){
                        copa.adicionarArbitro(arbitrosGenericos.get(i));
                    }
                }

                System.out.println("\nDeseja utilizar estadios genericos(G) ou estadios criados(C) para a Copa do Mundo 2018?(G/C)");
                text1 = ler.nextLine();
                if(text1.equals("G")){
                    for(i=0;i<4;i++){
                        copa.adicionarEstadio(estadiosGenericos.get(i));
                    }   
                }else if(text1.equals("C")){
                    for(i=0;i<4;i++){
                        copa.adicionarEstadio(estadios.get(i));
                    }
                }else{
                    System.out.println("\nComo nao foi inserido uma opcao valida, serao utilizados estadios genericos");
                    for(i=0;i<4;i++){
                        copa.adicionarEstadio(estadiosGenericos.get(i));
                    }
                }

                try{
                    copa.gerarGrupos();
                    copa.setEliminatorias(copa.simularFaseGrupos());
                    copa.simularSegundafase();
                    jogosAux = copa.getJogos();
                    jogos.addAll(jogos.size(), jogosAux);
                    System.out.println("\n"+copa.resultadoCopa());
                    
                }catch(NeedsMoreTeamsException e){
                    System.out.println("Quantidade de times insuficiente!");
                }catch(NeedsMorePlayersException e){
                    System.out.println("Quantidade de jogadores em um dos times insuficiente!");
                }catch(IncompleteChampionshipException e){
                    System.out.println("Campeonato completado incorretamente!");
                }
                
                System.out.print("\n\n\nTecle Enter para voltar ao menu!");
                ler.nextLine();
                break;
            case 4:// escrevendo em um arquivo txt o relatório da partida
                System.out.print("\nDigite os times que participaram do jogo");
                time1 = null;
                time2 = null;

                System.out.print("\nDigite o nome do primeiro time:");
                text1 = ler.nextLine();
                time1 = findTime(timesCriados, text1);

                System.out.print("\nDigite o nome do segundo time:");
                text2 = ler.nextLine();
                time2 = findTime(timesCriados, text2);

                System.out.print("\nDigite o dia que a partida ocorreu,caso nao saiba, digite 0:");
                aux1 = ler.nextInt();
                ler.nextLine();
                try {
                    partida = acharJogo(jogos, time1, time2, aux1);
                    
                    arquivoRelatorio = new FileWriter("./" + text1 + "-" + text2 + aux1 + ".txt");
                    gravarRelatorio = new PrintWriter(arquivoRelatorio);
                    
                    if (partida == null) {
                    
                    	jogosAux = acharJogo(jogos,time1,time2);
                    	if(jogosAux != null) {                  
                    		System.out.println("\nComo nao foram encontrados jogos dos times escolhidos no dia inserido, relataremos todos os jogos que ocorreram entre esses dois times;");
                    		for(i = 0; i < jogosAux.size();i++) {
                    			relatorioArq(gravarRelatorio,jogosAux.get(i));
                    		}
                    	}else {
                    		System.out.println("jogo nao encontrado!");
                    		break;                    		
                    	}
                    }
                    else
                    	relatorioArq(gravarRelatorio, partida);
                    
                    arquivoRelatorio.close();
                    System.out.println("\r\nSumula da partida criada no diretorio do projeto!");
                } catch (IOException e) {
                    System.out.println("Arquivo nao encontrado ou incapaz de ser criado!");
                }
                
                System.out.print("\n\n\nTecle Enter para voltar ao menu!");
                ler.nextLine();
                break;
            case 5:// adicionando estádio
                System.out.print("\nDigite o nome do Estadio:");
                nome1 = ler.nextLine();

                System.out.print("\nDigite o nome da Unidade Federativa do Estadio:");
                nome2 = ler.nextLine();

                estadio = new Estadio(nome1, nome2);
                estadios.add(estadio);
                
                System.out.print("\n\n\nTecle Enter para voltar ao menu!");
                ler.nextLine();
                break;
            case 6:// adicionando arbitro
                System.out.print("\nDigite o nome do Arbitro da Copa:");
                nome1 = ler.nextLine();
                System.out.print("\nDigite o nome da Unidade Federativa do Arbitro:");
                nome2 = ler.nextLine();
                System.out.print("\nDigite a funcao do Arbitro:");
                nome3 = ler.nextLine();
                arbi = new Arbitro(nome1, nome2,nome3);
                arbitros.add(arbi);

                System.out.print("\n\n\nTecle Enter para voltar ao menu!");
                ler.nextLine();
                break;
            case 7:
            	System.out.print("\nDigite o dia desejado:");
                time1 = null;
                
                aux1 = ler.nextInt();
                ler.nextLine();
                try {
                	jogosAux = acharJogo(jogos,aux1);                    
                    arquivoRelatorio = new FileWriter("./partidas_dia-"+ aux1 + ".txt");
                    gravarRelatorio = new PrintWriter(arquivoRelatorio);
                    
                    if(jogosAux.size() != 0){                    	
                    	for(i = 0; i < jogosAux.size();i++) {
                    		relatorioArq(gravarRelatorio,jogosAux.get(i));
                    	}
                    }
                    else {                    	
                    	System.out.println("Nenhum jogo foi encontrado no dia inserido!");
                    	break;                    		
                    }
                    
                    arquivoRelatorio.close();
                    System.out.println("\r\nSumula da partida criada no diretorio do projeto!");
                } catch (IOException e) {
                    System.out.println("Arquivo nao encontrado ou incapaz de ser criado!");
                }
                
                System.out.print("\n\n\nTecle Enter para voltar ao menu!");
                ler.nextLine();
                break;
            case 8:// finalizando programa
                System.out.print("\nFim do programa!\n");
                ler.close();
                return;
            default:
                System.out.print("\nA opcao desejada eh invalida, digite uma opcao existente.");
                break;
            }

        }
    }

    public static Time findTime(LinkedList<Time> times, String nome) {
        int i;
        int j = times.size();
        while (true) {
            for (i = 0; i < j; i++) {
                if (times.get(i).getNome().contentEquals(nome)) {
                    return times.get(i);
                }
            }
            if (i == j) {
                System.out.println(
                        "\nTime nao existente, por favor, digite um time existente ou crie um time com o nome desejado.");
                return null;
            }
        }
    }

    public static Estadio findEstadio(LinkedList<Estadio> estadios, String nome) {
        int i;
        int j = estadios.size();
        while (true) {
            for (i = 0; i < j; i++) {
                if (estadios.get(i).getNome().contentEquals(nome)) {
                    return estadios.get(i);
                }
            }
            if (i == j) {
                System.out.print(
                        "\n Estadio nao existente, por favor, digite um estadio existente ou crie um estadio com o nome desejado.");
                return null;
            }
        }
    }

    public static Jogo acharJogo(LinkedList<Jogo> jogos, Time time1, Time time2, int dia) {
        int i;
        Jogo jogo;
        for (i = 0; i < jogos.size(); i++) { // condição que checa se o jogo analisado no momento tem como integrantes o
                                      // time1 e o time2
            if ((jogos.get(i).getTime1().equals(time1) && jogos.get(i).getTime2().equals(time2))
                    || (jogos.get(i).getTime2().equals(time1) && jogos.get(i).getTime1().equals(time2))) {
                if (jogos.get(i).getDia() == dia) {
                    jogo = jogos.get(i);
                    return jogo;
                }
            }
        }
        return null;
    }

    public static void relatorioArq(PrintWriter gravar, Jogo partida) {
        gravar.println(partida.relatorio());
    }
//inserindo times do arquivo txt no programa
    public static void pularLinha() {
    	for(int i = 0; i<100;i++) {    		
    		System.out.print("\n");
    	}
    }
    public static LinkedList<Time> cadastroTimes(String nome) {
        FileReader arq;
        BufferedReader leitorBuffer;
        String linha, posicao, playerName;
        LinkedList<Time> times = new LinkedList<Time>();
        Jogador jogador;
        Time time;
        int i;

        try {
            arq = new FileReader(nome);
            leitorBuffer = new BufferedReader(arq);

            linha = leitorBuffer.readLine(); // lê a primeira linha, no caso será o nome do time
            // a variável "linha" recebe o valor "null" quando o processo
            // de repetição atingir o final do arquivo texto
            while (linha != null) {
                // TENHO A LINHA, AGORA DE ACORDO COM O MEU TXT, INTERPRETAR O TXT
                // PRIMEIRA COISA É O NOME DO TIME, DEPOIS PARA CADA JOGADOR COLOCAR A POSIÇÃO E
                // O NOME DO JOGADOR
                i = 0;
                time = new Time(linha);
                while (i < 23) {
                    linha = leitorBuffer.readLine();
                    posicao = linha;

                    linha = leitorBuffer.readLine();
                    playerName = linha;
                    
                    jogador = new Jogador(playerName,time,posicao);
                    time.addJogador(jogador);
                    i++;
                }
                times.add(time);
                linha = leitorBuffer.readLine(); // lê a próxima linha (no caso será o nome de um time ou nulo)
            }
            arq.close();
            return times;
        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage());
        }
        return null;
    }
    public static LinkedList<Jogo> acharJogo(LinkedList<Jogo> jogos,Time time1, Time time2){
        int i;
        Time auxtime1, auxtime2;
        Jogo auxjogo;
        LinkedList<Jogo> jogos2 = new LinkedList<Jogo>();
        try{
            for(i = 0; i < jogos.size(); i++){
                auxjogo = jogos.get(i);
                auxtime1 = auxjogo.getTime1();
                auxtime2 = auxjogo.getTime2();
                if((time1.equals(auxtime1) && time2.equals(auxtime2)) || (time1.equals(auxtime2) && time2.equals(auxtime1))){
                    jogos2.add(auxjogo);   
                }
            }
            return jogos2;
        }catch(IndexOutOfBoundsException e){
            return null;
        }
    }
    public static LinkedList<Jogo> acharJogo(LinkedList<Jogo> jogos,int dia){
    	int i;
    	Jogo jogo;
    	LinkedList<Jogo> jogos2 = new LinkedList<Jogo>();
    	try{
            for(i = 0; i < jogos.size(); i++){
                jogo = jogos.get(i);
                if(jogo.getDia() == dia) {
                	jogos2.add(jogo);
                }
            }
            return jogos2;
        }catch(IndexOutOfBoundsException e){
            return null;
        }
    }
}

// criar menu para o usuário
