package excessoes;
@SuppressWarnings("serial")
public class NeedsMorePlayersException extends Exception {
    @Override
    public String getMessage(){
        return "Numero de jogadores insuficiente!";
    }
}