package excessoes;

public class NoGameException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
    public String getMessage(){
        return "O jogo enviado nao e valido!";
    }
}