package excessoes;

@SuppressWarnings("serial")
public class NoTeamException extends Exception {
    @Override
    public String getMessage(){
        return "O time enviado nao existe!";
    }
}