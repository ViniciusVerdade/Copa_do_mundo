package excessoes;
@SuppressWarnings("serial")
public class MaxSizeException extends Exception {
    @Override
    public String getMessage(){
        return "Tamanho maximo atingido!";
    }
}